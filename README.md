# SETUP GUIDE #

### Clone the code

Clone the code on to your local environmnet.

### Install the application

1. Move on to electronic-store where all the modues are located.
2. mvn install
3. This will consume some time to install all the dependancies and build the jar and war files as defined on the PoM. If everything success you will get a green Success message.

### Run the application

1. mvn spring-boot:run -pl application
2. This will start the whole application with all the modues
3. Access the system on http://localhost:8080/estore/online/

### Run independant modues

1. mvn spring-boot:run -pl <module-name> -Dspring.profiles.active=dev
2. This will start the product module in development mode
3. Access the system on http://localhost:8080/estore/online

   Module names

 - product-module
 - product-service
 - order-module
 - order-service
 - invoice-module
 - invoice-service

Demo link : https://www.youtube.com/watch?v=VVongYU6dJc&t
