package com.thisara.invoice.web.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thisara.invoice.domain.Invoice;
import com.thisara.invoice.service.InvoiceService;

@Controller
@RequestMapping("/invoices")
public class InvoiceWebController {

	private static final Logger logger = Logger.getLogger(InvoiceWebController.class.getName());
	
	private final int TEST_USER_ID = 100102;
	
	@Value("${invoice.module.name}")
	private String moduleName = "invoices";
	
	@Value("${invoice.module.label}")
	private final String devProfileName = "dev";
	
	@Value("${invoice.module.dev.profile}")
	private final String moduleLabel = "My Invoices";
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private Environment env;
	
	@GetMapping("/getall")
	public ModelAndView getAllInvoices() {
		
		logger.info("Getting all invoices.");
		
		ModelAndView modelAndView = new ModelAndView();
		
		List<Invoice> invoicesList = invoiceService.findById(TEST_USER_ID);
		
		modelAndView.addObject("invoicesList", invoicesList);
		modelAndView.addObject("page", "invoiceList");
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
	
	@ModelAttribute
	public void singleModuleConfiguration(Model model) {
		
		String activeProfile = env.getProperty("spring.profiles.active");
		
		if(devProfileName.equals(activeProfile)) {
			model.addAttribute("singleModuleMode", "enabled");
			model.addAttribute("moduleName", moduleName);
			model.addAttribute("moduleLabel", moduleLabel);
		}
	}
}
