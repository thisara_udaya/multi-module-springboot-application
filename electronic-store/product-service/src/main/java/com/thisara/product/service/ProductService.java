package com.thisara.product.service;

import java.util.List;

import com.thisara.product.domain.Product;

public interface ProductService {

	public List<Product> getAllProducts();
}
