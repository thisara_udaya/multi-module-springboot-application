package com.thisara.product.rest.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thisara.product.domain.Product;
import com.thisara.product.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductRestController {

	private static final Logger logger = Logger.getLogger(ProductRestController.class.getName());
	
	private final ProductService productService;
	
	@Autowired
	public ProductRestController(ProductService productService) {
		
		logger.info("product service initialized.");
		
		this.productService = productService;
	}
	
	@GetMapping("/getallproducts")
	public List<Product> getAllProducts(){
		
		logger.info("getting all products.");
		
		return productService.getAllProducts();
	}
}
