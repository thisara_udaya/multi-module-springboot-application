package com.thisara.product.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan("com.thisara.product")
@Profile("dev")
public class ProductApplication {
	
	public static void main(String args[]) {
		
		SpringApplication.run(ProductApplication.class, args);
	}
}
