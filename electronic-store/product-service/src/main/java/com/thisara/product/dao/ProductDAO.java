package com.thisara.product.dao;

import java.util.List;

import com.thisara.product.domain.Product;

public interface ProductDAO{
	
	public List<Product> findAll();
	
}
