package com.thisara.product.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.thisara.product.domain.Product;

@Repository
@Qualifier("jSON")
public class ProductDAOImpl implements ProductDAO{
	
	private static final Logger logger = Logger.getLogger(ProductDAOImpl.class.getName());
	
	@Value("classpath:json-data/products.json")
	private Resource resource;
	
	@SuppressWarnings("unchecked")
	public List<Product> findAll(){
		
		logger.info("getting all products.");
		
		List<Product> productsList = new ArrayList<Product>();
		
		try {
			File productsFile = resource.getFile();
			
			JSONParser jsonParser = new JSONParser();
	
			try (FileReader reader = new FileReader(productsFile)) {
				
	            Object obj = jsonParser.parse(reader);
	 
	            JSONArray productsJson = (JSONArray) obj;

	            productsJson.forEach( item -> { 
	            	Product product = parseProduct( (JSONObject) item );
	            	productsList.add(product);
	            } );
	 
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }

		} catch (IOException e) {
			e.printStackTrace();
		}

		return productsList;
	}
	
	private static Product parseProduct(JSONObject productJson) {
		
		Product product = new Product();
		
        JSONObject productObject = (JSONObject) productJson.get("product");
         
        String productId = (String) productObject.get("product_id"); 
        String productName = (String) productObject.get("product_name");  
        String productUom = (String) productObject.get("product_uom");   
        String productPrice = (String) productObject.get("product_price");    
        String productPriceCurrency = (String) productObject.get("product_price_currency");    
        String productImageName = (String) productObject.get("product_image_name");
        
        product.setProductId(Integer.parseInt(productId));
        product.setProductName(productName);
        product.setProductUom(productUom);
        product.setProductPrice(Double.parseDouble(productPrice));
        product.setProductPriceCurrency(productPriceCurrency);
        product.setProductImageName(productImageName);
        
        return product;
    }

}
