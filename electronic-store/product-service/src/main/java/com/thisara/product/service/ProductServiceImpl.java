package com.thisara.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.thisara.product.dao.ProductDAO;
import com.thisara.product.domain.Product;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	@Qualifier("jSON")
	private ProductDAO productDAO;
	
	
	public List<Product> getAllProducts(){
		
		List<Product> productList = productDAO.findAll();

		return productList;
	}
}
