package com.thisara.product.domain;

public class Product {

	private int productId;
	private String productName;
	private String productUom;
	private double productPrice;
	private String productImageName;
	private String productPriceCurrency;
	
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductUom() {
		return productUom;
	}
	public void setProductUom(String productUom) {
		this.productUom = productUom;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductPriceCurrency() {
		return productPriceCurrency;
	}
	public void setProductPriceCurrency(String productPriceCurrency) {
		this.productPriceCurrency = productPriceCurrency;
	}
	
	public String getProductImageName() {
		return productImageName;
	}
	public void setProductImageName(String productImageName) {
		this.productImageName = productImageName;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productUom=" + productUom
				+ ", productPrice=" + productPrice + ", productImageName=" + productImageName
				+ ", productPriceCurrency=" + productPriceCurrency + "]";
	}
}
