package com.thisara.main;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ElectronicStoreController {

	private static Logger logger = Logger.getLogger(ElectronicStoreController.class.getName());
	
	@GetMapping("/")
	public ModelAndView prepareIndex() {
		
		logger.info("Loading page - home");
		
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("page", "home");
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
	
	@GetMapping("/{page}")
	public ModelAndView prepareIndex(@PathVariable("page")String page) {
		
		logger.info("Loading page - " + page);
		
		ModelAndView modelAndView = new ModelAndView();
		
		/** page can be mapped to internal actual page names. Ignored since spring url mappings used. */
		
		modelAndView.addObject("page", page);
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
}
