package com.thisara.main;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan( basePackages = {
		"com.thisara.order",
		"com.thisara.product",
		"com.thisara.invoice",
		"com.thisara.main"
})
public class ElectonicStoreApplication {

	private static final Logger logger = Logger.getLogger(ElectonicStoreApplication.class.getName());
	
	public static void main(String[] args) {
		
		SpringApplication.run(ElectonicStoreApplication.class, args);
		
		logger.info(ElectonicStoreApplication.class.getName() + " started...!");
	}

}
