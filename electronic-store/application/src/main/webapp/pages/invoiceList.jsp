<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Invoices</title>
<link href="/estore/online/css/list.styles.css" rel="stylesheet" />
</head>
<body>
	<div>
		<h2>My Invoices</h2>
		<div class="table">
			<div class="tableRow">
				<div class="tableHead">
					<span style="font-weight: bold;">Invoice ID</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Order ID</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Invoice Date</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Invoice Total</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Currency</span>
				</div>
			</div>
			<c:forEach items="${invoicesList}" var="invoice">
				<div class="tableRow">
					<div class="tableCell">
						<c:out value="${invoice.invoiceId}" />
					</div>
					<div class="tableCell">
						<c:out value="${invoice.orderId}" />
					</div>
					<div class="tableCell">
						<c:out value="${invoice.invoiceDate}" />
					</div>
					<div class="tableCell">
						<c:out value="${invoice.invoiceTotal}" />
					</div>
					<div class="tableCell">
						<c:out value="${invoice.invoiceCurrency}" />
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>