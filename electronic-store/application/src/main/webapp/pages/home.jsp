<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome to Electronic Store</title>
</head>
<body>
		<div class="container">
		<div class="section">
			<h2>Welcome to the Electronic Store</h2>
			
			<p>This is a sample application developer to demonstrate the multi-module configuration of an application. This application is built on SpringBoot.</p>
			
			<p>The application modules are capable of invoking other modules via maven dependancies or Restful API services.</p>

		</div>
	</div>
</body>
</html>