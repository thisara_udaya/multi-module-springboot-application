<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Product Catalog</title>
<link href="/estore/online/css/catalog.styles.css" rel="stylesheet" />
</head>
<body>
	<div>
		<h2>Product Catalog</h2>
		<main class="grid">
			<c:forEach items="${productsList}" var="product">
				<article>

					<div class="imgContainer">
						<img class="img" src="/estore/online/images/${product.productImageName}"
							alt="${product.productImageName}">
					</div>
					<div class="text">
						<h3>
							<c:out value="${product.productName}" />
						</h3>
						<p>
							<c:out value="${product.productId}" />
						</p>
						<p>
							<c:out value="${product.productPrice}" />
							<c:out value="${product.productPriceCurrency}" />
							|
							<c:out value="${product.productUom}" />
						</p>
						<button>Add</button>
					</div>
				</article>
			</c:forEach>
		</main>
	</div>
</body>
</html>