<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Electronic Store</title>
<link href="/estore/online/css/index.styles.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Electronic Store</h1>
        </div>
        <div class="wrapper clearfix">
            <div class="nav">
            	<c:if test = "${singleModuleMode ne 'enabled'}">
                <ul>
                    <li><a href="/estore/online/home">Home</a></li>
                    <li><a href="/estore/online/products/getall">Product Catalog</a></li>
                    <li><a href="/estore/online/orders/getall">My Orders</a></li>
                    <li><a href="/estore/online/invoices/getall">My Invoices</a></li>
                </ul>
                </c:if>
                <c:if test = "${singleModuleMode eq 'enabled'}">
                	<ul>
                    	<li><a href="/estore/online/${moduleName}/getall">${moduleLabel}</a></li>
                    </ul>
                </c:if>
            </div>
            <div class="section">
				<jsp:include page="${page}.jsp"></jsp:include>
            </div>
        </div>
        <div class="footer">
            <p>copyright &copy; 2020 - Thisara Alawala - mytechblogs.com</p>
        </div>
    </div>
</body>
</html>