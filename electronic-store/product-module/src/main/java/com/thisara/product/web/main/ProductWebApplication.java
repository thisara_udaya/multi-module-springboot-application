package com.thisara.product.web.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan("com.thisara.product")
@Profile("dev")
public class ProductWebApplication {
	
	public static void main(String args[]) {
		SpringApplication.run(ProductWebApplication.class, args);
	}
}
