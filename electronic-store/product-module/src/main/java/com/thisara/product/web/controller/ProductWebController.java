package com.thisara.product.web.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thisara.product.domain.Product;
import com.thisara.product.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductWebController {

	private static final Logger logger = Logger.getLogger(ProductWebController.class.getName());
	
	@Value("${product.module.name}")
	private String moduleName;
	
	@Value("${product.module.dev.profile}")
	private String devProfileName;
	
	@Value("${product.module.label}")
	private String moduleLabel;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private Environment env;
	
	@RequestMapping("/getall")
	public ModelAndView getOrders() {
		
		logger.info("getting all orders.");
		
		List<Product> productsList = productService.getAllProducts();
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("productsList", productsList);
		modelAndView.addObject("page", "productList");
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
	
	@ModelAttribute
	public void singleModuleConfiguration(Model model) {
		
		String activeProfile = env.getProperty("spring.profiles.active");
		
		if(devProfileName.equals(activeProfile)) {
			model.addAttribute("singleModuleMode", "enabled");
			model.addAttribute("moduleName", moduleName);
			model.addAttribute("moduleLabel", moduleLabel);
		}
	}
}
