package com.thisara.order.web.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan("com.thisara.order")
@Profile("dev")
public class OrderWebApplication {

	public static void main(String args[]) {
		
		SpringApplication.run(OrderWebApplication.class, args);
	}
}
