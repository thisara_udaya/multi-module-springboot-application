package com.thisara.order.web.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thisara.order.domain.Order;
import com.thisara.order.service.OrderService;

@Controller
@RequestMapping("/orders")
public class OrderWebController {

	private static final Logger logger = Logger.getLogger(OrderWebController.class.getName());
	
	private final int TEST_USER_ID = 100102;
	
	@Value("${order.module.name}")
	private String moduleName;
	
	@Value("${order.module.dev.profile}")
	private String devProfileName;
	
	@Value("${order.module.label}")
	private String moduleLabel;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private Environment env;
	
	@RequestMapping("/getall")
	public ModelAndView getOrders() {
		
		logger.info("Getting all orders.");
		
		List<Order> ordersList = orderService.findById(TEST_USER_ID);
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("ordersList", ordersList);
		modelAndView.addObject("page", "orderList");
		modelAndView.setViewName("index");
		
		return modelAndView;
	}
	
	@ModelAttribute
	public void singleModuleConfiguration(Model model) {
		
		String activeProfile = env.getProperty("spring.profiles.active");
		
		if(devProfileName.equals(activeProfile)) {
			model.addAttribute("singleModuleMode", "enabled");
			model.addAttribute("moduleName", moduleName);
			model.addAttribute("moduleLabel", moduleLabel);
		}
	}
}
