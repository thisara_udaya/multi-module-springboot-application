<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Orders</title>
<link href="/estore/online/css/list.styles.css" rel="stylesheet" />
</head>
<body>
	<div>
		<h2>My Orders</h2>
		<div class="table">
			<div class="tableRow">
				<div class="tableHead">
					<strong>Order ID</strong>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Order Date</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Order Status</span>
				</div>
				<div class="tableHead">
					<span style="font-weight: bold;">Order Items</span>
				</div>
			</div>
			<c:forEach items="${ordersList}" var="order">
				<div class="tableRow">
					<div class="tableCell">
						<c:out value="${order.orderId}" />
					</div>
					<div class="tableCell">
						<c:out value="${order.orderDate}" />
					</div>
					<div class="tableCell">
						<c:out value="${order.orderStatus}" />
					</div>
					<div class="tableCell">
						<c:forEach items="${order.orderItems}" var="orderItems">
							<div>
								<b>Product ID : </b>
								<c:out value="${orderItems.productId}" />
								<b>Order Qty : </b>
								<c:out value="${orderItems.itemQuantity}" />
								<b>Product Name : </b>
								<c:out value="${orderItems.productName}" />
							</div>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>