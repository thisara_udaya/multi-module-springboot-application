package com.thisara.order.service;

import java.util.List;

import com.thisara.order.domain.Order;

public interface OrderService {

	public List<Order> findById(int userId); 
}
