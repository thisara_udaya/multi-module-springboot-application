package com.thisara.order.rest.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thisara.order.domain.Order;
import com.thisara.order.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderRestController {

	private static final Logger logger = Logger.getLogger(OrderRestController.class.getName());
	
	private final int TEST_USER = 100102;
	
	@Autowired
	private OrderService orderService;
	
	@GetMapping("/getallorders")
	public List<Order> getOrders() {
		
		logger.info("getting all orders.");
		
		return orderService.findById(TEST_USER);
	}
}
