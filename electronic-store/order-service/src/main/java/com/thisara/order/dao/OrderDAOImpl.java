package com.thisara.order.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.thisara.order.domain.Order;
import com.thisara.order.domain.OrderItem;

@Repository
@Qualifier("jSON")
public class OrderDAOImpl implements OrderDAO{
	
	private static final Logger logger = Logger.getLogger(OrderDAOImpl.class.getName());

	@Value("classpath:json-data/orders.json")
	private Resource resource;

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> findById(int userId) {

		logger.info("Getting all orders.");
		
		List<Order> ordersList = new ArrayList<Order>();
		
		try {
			File ordersFile = resource.getFile();
			
			JSONParser jsonParser = new JSONParser();
	
			try (FileReader reader = new FileReader(ordersFile)) {
				
	            Object obj = jsonParser.parse(reader);
	 
	            JSONArray ordersJson = (JSONArray) obj;

	            ordersJson.forEach( item -> { 
	            	
	            	Order order = parseOrder( (JSONObject) item );
	            	
	            	if(order.getUser_id() == userId) {
	            		ordersList.add(order);
	            	}
	            } );
	 
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ordersList;
	}
	
	@SuppressWarnings("unchecked")
	private static Order parseOrder(JSONObject orderJson) {
		
		Order order = new Order();
		List<OrderItem> orderItems = new ArrayList<OrderItem>();
		
        JSONObject productObject = (JSONObject) orderJson.get("order");
         
        String orderId = (String) productObject.get("order_id"); 
        String orderDate = (String) productObject.get("order_date");  
        String orderStatus = (String) productObject.get("order_status");   
        String userId = (String) productObject.get("user_id");
        
        JSONArray orderItemsJson = (JSONArray) productObject.get("order_items");    
        
        orderItemsJson.forEach( item -> {
        
        	OrderItem orderItem = new OrderItem();
        	JSONObject orderItemObject = (JSONObject)item;
        	
        	String itemQuantity = (String)(orderItemObject.get("order_item_quantity"));
            String productId = (String)(orderItemObject.get("product_id"));
            String productName = (String)(orderItemObject.get("product_name"));
            
            orderItem.setItemQuantity(Double.parseDouble(itemQuantity));
            orderItem.setProductId(Integer.parseInt(productId));
            orderItem.setProductName(productName);
            
            orderItems.add(orderItem);
        });

        order.setOrderId(Integer.parseInt(orderId));
        order.setOrderDate(orderDate);
        order.setOrderStatus(orderStatus);
        order.setUser_id(Integer.parseInt(userId));
        order.setOrderItems(orderItems);

        return order;
    }
}
