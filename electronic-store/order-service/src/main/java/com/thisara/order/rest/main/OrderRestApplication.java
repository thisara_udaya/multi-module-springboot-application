package com.thisara.order.rest.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan("com.thisara.order")
@Profile("dev")
public class OrderRestApplication {

	public static void main(String args[]) {
		
		SpringApplication.run(OrderRestApplication.class, args);
	}
}
