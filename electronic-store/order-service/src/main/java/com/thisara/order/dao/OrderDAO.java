package com.thisara.order.dao;

import java.util.List;

import com.thisara.order.domain.Order;

public interface OrderDAO {

	public List<Order> findById(int userId);
}
