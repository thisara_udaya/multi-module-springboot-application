package com.thisara.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.thisara.order.dao.OrderDAO;
import com.thisara.order.domain.Order;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	@Qualifier("jSON")
	private OrderDAO OrderDAO;
	
	@Override
	public List<Order> findById(int userId) {
		
		return OrderDAO.findById(userId);
	}

}
