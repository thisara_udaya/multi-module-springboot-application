package com.thisara.order.domain;

public class OrderItem {

	private int orderItemId;
	private double itemQuantity;
	private int productId;
	private String productName;

	public int getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(int orderItemId) {
		this.orderItemId = orderItemId;
	}

	public double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "OrderItem [orderItemId=" + orderItemId + ", itemQuantity=" + itemQuantity + ", productId=" + productId
				+ ", productName=" + productName + "]";
	}
}
