package com.thisara.invoice.rest.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@ComponentScan("com.thisara.invoice")
@Profile("dev")
public class InvoiceApplication {

	public static void main(String []args) {
		SpringApplication.run(InvoiceApplication.class, args);
	}
}
