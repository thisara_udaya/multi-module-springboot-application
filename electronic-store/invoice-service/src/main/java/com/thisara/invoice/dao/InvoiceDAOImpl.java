package com.thisara.invoice.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.thisara.invoice.domain.Invoice;

@Repository
public class InvoiceDAOImpl implements InvoiceDAO{
	
	private static final Logger logger = Logger.getLogger(InvoiceDAOImpl.class.getName());
	
	@Value("classpath:json-data/invoices.json")
	private Resource resource;

	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findById(int userId) {
		
		logger.info("Getting all invoices of - " + userId);
		
		List<Invoice> invoicesList = new ArrayList<Invoice>();
		
		try {
			File invoicesFile = resource.getFile();
			
			JSONParser jsonParser = new JSONParser();
	
			try (FileReader reader = new FileReader(invoicesFile)) {
				
	            Object obj = jsonParser.parse(reader);
	 
	            JSONArray invoicesJson = (JSONArray) obj;

	            invoicesJson.forEach( item -> { 
	            	
	            	Invoice invoice = parseInvoice( (JSONObject) item );
	            	
	            	if(invoice.getUserId() == userId) {
	            		
	            		invoicesList.add(invoice);
	            	}
	            } );
	 
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }

		} catch (IOException e) {
			e.printStackTrace();
		}

		return invoicesList;
	}
	
	private static Invoice parseInvoice(JSONObject productJson) {
		
		Invoice invoice = new Invoice();
		
        JSONObject invoiceObject = (JSONObject) productJson.get("invoice");
         
        String invoiceId = (String) invoiceObject.get("invoice_id"); 
        String orderId = (String) invoiceObject.get("order_id");  
        String invoiceDate = (String) invoiceObject.get("invoice_date");   
        String invoiceTotal = (String) invoiceObject.get("invoice_total");    
        String invoiceCurrency = (String) invoiceObject.get("invoice_currency");   
        String userId = (String) invoiceObject.get("user_id");   
        
        invoice.setInvoiceId(Integer.parseInt(invoiceId));
        invoice.setOrderId(Integer.parseInt(orderId));
        invoice.setInvoiceTotal(Double.parseDouble(invoiceTotal));
        invoice.setInvoiceDate(invoiceDate);
        invoice.setInvoiceCurrency(invoiceCurrency);
        invoice.setUserId(Integer.parseInt(userId));
        
        return invoice;
    }
}
