package com.thisara.invoice.service;

import java.util.List;

import com.thisara.invoice.domain.Invoice;

public interface InvoiceService {

	public List<Invoice> findById(int userId);
}
