package com.thisara.invoice.dao;

import java.util.List;

import com.thisara.invoice.domain.Invoice;

public interface InvoiceDAO {

	public List<Invoice> findById(int userId);
}
