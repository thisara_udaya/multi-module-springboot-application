package com.thisara.invoice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thisara.invoice.dao.InvoiceDAO;
import com.thisara.invoice.domain.Invoice;

@Service
public class InvoiceServiceImpl implements InvoiceService{

	@Autowired
	private InvoiceDAO invoiceDAO;
	
	@Override
	public List<Invoice> findById(int userId) {
		
		return invoiceDAO.findById(userId);
	}

}
