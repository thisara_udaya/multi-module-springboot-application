package com.thisara.invoice.rest.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thisara.invoice.domain.Invoice;
import com.thisara.invoice.service.InvoiceService;

@RestController
@RequestMapping("/invoice")
public class InvoiceRestController {

	private static final Logger logger = Logger.getLogger(InvoiceRestController.class.getName());
	
	private final int TEST_USER = 100102;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@GetMapping("/getallinvoices")
	public List<Invoice> getAllInvoices(){
		
		logger.info("getting all invoices.");
		
		return invoiceService.findById(TEST_USER);
	}
}
